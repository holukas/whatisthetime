import datetime as dt

import pytz


def getcurrenttime(tz: str, fmt: str = '%Y-%m-%d %H:%M:%S %Z%z') -> str:
    tz = pytz.timezone(tz)
    tz_currenttime_dt = dt.datetime.now(tz)
    return tz_currenttime_dt.strftime(fmt)


def main():
    utc_str = getcurrenttime(tz='UTC')
    print(f"Current time UTC (Coordinated Universal Time): {utc_str}")

    tz = 'Europe/Zurich'
    cet_str = getcurrenttime(tz=tz)
    print(f"Current time in {tz}: {cet_str}")


if __name__ == '__main__':
    main()
