# Changelog

## v0.0.1 | 4 Nov 2021

### Initial Release
- Script returns current time as UTC and local time for `Europe/Zurich`
