# whatisthetime

## Description
Get current time of different time zones

## Packages
Based on time zone functions from `pytz`:
- https://pythonhosted.org/pytz/
- https://pypi.org/project/pytz/
